
# Installing Desktop-ish

Run the CompileGambasexe script in the source folder to compile the .gambas executable or load the project into gambas IDE and do it manually if you know how.

Once you have the Desktop-ish.gambas file you can move it to any location you desire.

Manually set up launchers for the executable or double click it and select the Options (right click)
On the options page there is a "Make Launcher" button, this will add a launcher to your system menu.

Also on the Options page there is a "Run at system start" option that will set it to run as your desktop loads.
