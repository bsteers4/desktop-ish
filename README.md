<center>
# Desktop (ish)

### A desktop launcher application written in Gambas basic
#### Written by Bruce Steers

This Applicaiton is currently WIP (a work in progress)\
Various things may not function as expected while in development.

</center>
<hr>
<br>
### Intro:

I hate that on gnome desktop and some others you cannot get your desktop icons on your desktop, or your panel launchers.

I use my launchers a lot and have many custom "Actions" set.
<br>
* Wouldn't it be good if on gnome you could see your $HOME/Desktop icons on your desktop and have a launcher panel if you want?
<p>
* Also wouldn't it be cool if the desktop icons all showed their alternative actions when right clicking like they do in a panel?
<p>
* Also wouldn't an editor for the custom Actions be handy?
<p>
* Also wouldn't it be handy to create custom menus to do whatever you want?
<p>
* Also wouldn't it be handy if you right clicked your Gambas3 icon and it displayed the recent file list?
<p>
* Also wouldn't it be handy if you used various different desktops and didn't want the application to run on some desktops?
<p>
* All this is possible with this Application.
<br>


### Todo list:
* Test and debug

<br>

<img src="http://bws.org.uk/images/Desktop-ish.png">


### Bugs:

Report bugs to <a href="bsteers4@gmail.com">My email addy</a> or find me on the <a href="https://forum.gambas.one/index.php">gambas-one</a> forum.

<hr>

## Special thanks to...

* **Benoit Minisini**, The creator of gambas basic. Thank you for Gambas and for any code i have shamelesly ripped into my software :)

<p>

