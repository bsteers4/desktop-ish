#!/usr/bin/env bash

GetGUI() {
  if [ -z $1 ]; then
  RES=$(echo -e "Use \"gb.gui\"\nPrint Env[\"GB_GUI\"]\n" | env GB_GUI="" gbs3 - 2>/dev/null)
else
RES=$(echo -e "Use \"gb.gui\"\nIf Args[1] = Env[\"GB_GUI\"] Then Quit 0\nQuit 1\n" | env GB_GUI="gb.$1" gbs3 - gb.$1 2>/dev/null)
fi
}

# Get application directory and exe name
WD=${0%/*}
AppName=${WD##*/}
eval $(cat ./.settings|grep Path=)
if [ -z "$Path" ]; then eval $(cat ./.project|grep Path=); fi
if [ -z "$Path" ]; then Path=$AppName".gambas"; fi

# get desktop path
source $HOME/.config/user-dirs.dirs
DESK=$XDG_DESKTOP_DIR

# ask what launcher icons to make
REP=$(zenity --list --height=200 --width=300 --checklist --text="Compile $AppName in\n$WD\n\nSelect launcher icons to create after compiling" --column=Enable --column="Make launcher type" "" "Make menu" "" "Make desktop icon")
ERR=$?

  if [ $ERR -eq 1 ]; then 
  echo "User canceled"
  exit; 
  fi

DODTOP=0; DOMENU=0

if [ ! -z "$REP" ]; then
 if [  "${REP%|*}" == "Make menu" ]; then DOMENU=1; fi
 if [  "${REP#*|}" == "Make desktop icon" ]; then DODTOP=1; fi
fi

 if [ $[$DOMENU+$DODTOP] -eq 0 ]; then ERR=1; fi

cd "$WD"

# find what flags to pass gbc3 by reading the .project info
ControlPublic=0
ModulePublic=0
eval $(cat ./.project | grep "ControlPublic") 2>/dev/null
eval $(cat ./.project | grep "ModulePublic") 2>/dev/null

KeepDebugInfo=1
eval $(cat .project|grep KeepDebugInfo)

echo "Compiling gambas executable..."
FLG="-wax"
MODE="-f"
CHK=$(gbc3 --help| grep public-module| awk {'print $1'})
if [[ $CHK != "-f" ]]; then
MODE="-"
fi
if [ $KeepDebugInfo -eq 1 ]; then FLG="$FLG"g; fi
if [ $ControlPublic -eq 1 ]; then FLG="$FLG "$MODE"public-control"; fi
if [ $ModulePublic -eq 1 ]; then FLG="$FLG "$MODE"public-module"; fi

# compile the application
gbc3 $FLG
gba3

if [ $ERR -eq 1 ]; then exit; fi 

echo "Making launcher..."
# select launcher icon alternative actions.
OPTS=$(zenity --forms --text="Select Alternative Actions for the icon.(options with right click)" --add-combo="GUI Toolkits (select gtk/qt5/etc)" --combo-values="|Add All|Add Non-default" --add-combo="Gambas Project Edit (open in gambas IDE)" --combo-values="|Add Project Edit")

if [ $? -eq 1 ]; then zenity --info --width=400 --text="Create icon canceled."; exit; fi

if [ -z "$OPTS" ]; then OPTS=" | "; fi
GUIMODE="${OPTS%|*}"
EDTMODE="${OPTS#*|}"

if [[ $GUIMODE != " " ]]; then
GUIS=""
GetGUI
DEF=$RES
ACTS=""

for s in gtk gtk3 qt4 qt5 qt6;do 
if [ ! -e "/usr/lib/gambas3/gb.$s.component" ]; then continue; fi
if [[ "$GUIMODE" != "Add All" ]]; then
  if [[ "gb.$s" == "$DEF" ]]; then continue; fi; 
fi
GetGUI $s
if [ $? -eq 0 ]; then 
SS=${s^^}
if [ "$SS" == "GTK" ]; then SS="GTK2"; fi
GUIS=$GUIS"RUN_$SS;"
ACTS="$ACTS\n[Desktop Action RUN_$SS]\nName=Run with $SS\nExec=env GB_GUI=gb.$s '$WD/$Path'\n"
fi
done
fi

if [[ $EDTMODE != " " ]]; then
GUIS=$GUIS"EDITG;"
ACTS="$ACTS\n[Desktop Action EDITG]\nName=Edit with Gambas3\nExec=gambas3 '$WD'\n"
fi

if [ -z "$ACTS" ]; then
  echo "no actions to add"
else
  ACTS="Actions=${GUIS%*;}\n$ACTS"
  echo "Adding Actions=${GUIS%*;}"
fi

# try to get icon from .project file or use default
IC=$(cat "$WD/.project" |grep Icon=)

if [ -z "$IC" ]; then Icon=".icon.png"; else Icon=${IC##*=}; fi

if [ $DOMENU -eq 1 ]; then
  RES=$(zenity --list --hide-header --text="Choose menu category." --height=300 --column=Category Accesory Graphics Internet Office Development Sound Video "System Tools" "Desktop settings" "System Settings")
fi
if [ $? -eq 1 ]|[ -z $RES ]; then
zenity --info --width=400 --text="Menu item canceled."
DOMENU=""
CATS=""
fi

if [ "$RES" == "Accesory" ]; then CATS="Application;Utility"
elif [ "$RES" == "Internet" ]; then CATS="Application;Network;Internet;Web"
elif [ "$RES" == "Audio" ]; then CATS="Application;AudioVideo;Player;Audio"
elif [ "$RES" == "Video" ]; then CATS="Application;AudioVideo;Player;Video"
elif [ "$RES" == "System Tools" ]; then CATS="Application;System"
elif [ "$RES" == "Desktop Settings" ]; then CATS="Application;Settings;DesktopSettings"
elif [ "$RES" == "System Settings" ]; then CATS="Application;System;Settings"
else 
CATS="$RES"
fi

if [ ! -z "$CATS" ]; then CATS="Categories=$CATS\n"; fi

TEXT="[Desktop Entry]\nType=Application
Name=$AppName
Icon=$WD/$Icon
Exec=$WD/$Path
$CATS$ACTS"

MSG="All Done"
if [ $DODTOP -eq 1 ]; then
echo -e "$TEXT" >"$DESK/$AppName.desktop"
chmod +x "$DESK/$AppName.desktop"
MSG="$MSG\n$XDG_DESKTOP_DIR/$AppName.desktop created."
fi
if [ $DOMENU -eq 1 ]; then
echo -e "$TEXT" >"$HOME/.local/share/applications/$AppName.desktop"
MSG="$MSG\n$RES menu created."
#chmod +x "$HOME/.local/share/applications/$AppName.desktop"
fi

zenity --info --width=400 --text="$MSG"

