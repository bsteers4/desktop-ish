## On this page you can define custom menus to add to icons. ##

### Different types of menu you can add... ###

* New Menu: a top level menu.\
  "Rules" are added to control if the menu shows. rules can be extension type or mime type. If the file is a .desktop file the Exec line is checked.\
   Ie. A .desktop file that links to gambas3 will be application/x-gambas3.

* New Submenu: a menu conaining other menu items.

* New Auto-menu: Runs a command/script to create the menu list.\
  An Auto-menu must run a script or command that returns a list. Each line must be in the format of..
  Command to run||[Menu text]\
  Menu text is optional and if not given the command is shown as the menu text.

* New menu item: A normal menu item.\
  For a normal menu item just set the menu text and the command to run upon selection.

